APP_NAME=buildkit-demo
IMAGE_REGISTRY=imrenagi
IMAGE_NAME=$(IMAGE_REGISTRY)/$(APP_NAME)
GIT_COMMIT=$(shell git rev-parse --short HEAD)
IMAGE_TAG=$(GIT_COMMIT)

.PHONY: build.image
build.image: 
	# docker build -t $(IMAGE_NAME):$(IMAGE_TAG) .

	DOCKER_BUILDKIT=1 docker build --cache-from $(IMAGE_NAME):latest -t $(IMAGE_NAME):$(IMAGE_TAG) --build-arg BUILDKIT_INLINE_CACHE=1 .

	# DOCKER_BUILDKIT=1 docker build --target builder --cache-from $(IMAGE_NAME):builder --tag $(IMAGE_NAME):builder --build-arg BUILDKIT_INLINE_CACHE=1 .
	# DOCKER_BUILDKIT=1 docker build --cache-from $(IMAGE_NAME):builder --cache-from $(IMAGE_NAME):latest -t $(IMAGE_NAME):$(IMAGE_TAG) --build-arg BUILDKIT_INLINE_CACHE=1 .
	docker tag $(IMAGE_NAME):$(IMAGE_TAG) $(IMAGE_NAME):latest

.PHONY: release.image
release.image: 
	# docker push $(IMAGE_NAME):builder
	docker push $(IMAGE_NAME):$(IMAGE_TAG)
	docker push $(IMAGE_NAME):latest
