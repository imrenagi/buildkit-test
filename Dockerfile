FROM alpine:3.10 
RUN apk update 
RUN apk add --no-cache ca-certificates tzdata && update-ca-certificates
RUN apk add curl
RUN apk add --update alpine-sdk
USER root

# Install Python
RUN apk add --update
RUN apk add py3-setuptools python3 py3-pip
#RUN ln -s /usr/bin/python3 python
RUN pip3 install --upgrade pip
RUN python3 -V
RUN pip --version

# Install helm
ENV HELM_VERSION="v2.14.1"
RUN wget -q http://storage.googleapis.com/kubernetes-helm/helm-${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm && \
      chmod +x /usr/local/bin/helm

# Install gcloud
RUN curl https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz > /tmp/google-cloud-sdk.tar.gz
# Installing the package
RUN mkdir -p /usr/local/gcloud \
  && tar -C /usr/local/gcloud -xvf /tmp/google-cloud-sdk.tar.gz \
  && /usr/local/gcloud/google-cloud-sdk/install.sh
# Adding the package path to local
ENV PATH $PATH:/usr/local/gcloud/google-cloud-sdk/bin

# Install aws cli
RUN pip install awscli --upgrade --user
# add aws cli location to path
ENV PATH=~/.local/bin:$PATH

WORKDIR /
COPY . .
RUN chmod a+x exec.sh

CMD ["./exec.sh"]

# FROM alpine:3.10
# RUN apk update 
# WORKDIR /

# COPY --from=builder /exec.sh .

# CMD ["./exec.sh"]